import React, { Component } from 'react';

export default class Glasses_Item extends Component {
  render() {
    let { url } = this.props.data;

    return (
      <div
        onClick={() => {
          this.props.handleOnClick(this.props.data);
        }}
        className="border col-3"
      >
        <img style={{ width: '100%' }} src={`/img/${url}`} alt="" />
      </div>
    );
  }
}
