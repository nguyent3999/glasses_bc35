import React, { Component } from 'react';
import { dataGlasses } from './dataGlasses';
import Glasses_Item from './Glasses_Item';
import Glasses_Model from './Glasses_Model';

export default class Glasses extends Component {
  state = {
    glassesArr: dataGlasses,
    glasseDetail: {},
  };

  renderGlassesItem = () => {
    return this.state.glassesArr.map((item, index) => {
      return (
        <Glasses_Item
          handleOnClick={this.handleGlassesItemClick}
          data={item}
          key={index}
        />
      );
    });
  };

  handleGlassesItemClick = (glasses) => {
    this.setState({
      glasseDetail: glasses,
    });
  };
  render() {
    return (
      <div className="container">
        <div>
          <Glasses_Model detail={this.state.glasseDetail} />
        </div>
        <div className="row my-3">{this.renderGlassesItem()}</div>
      </div>
    );
  }
}
