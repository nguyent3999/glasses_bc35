import React, { Component } from 'react';

export default class Glasses_Model extends Component {
  render() {
    let { url, name, price, desc } = this.props.detail;
    return (
      <>
        <h2>TRY GLASSES APP ONLINE</h2>
        <div className="row justify-content-between">
          <div className="col-5">
            <img
              style={{ width: '100%', position: 'relative' }}
              src="/img/glassesImage/model.jpg"
              alt=""
            />

            <img
              style={{
                width: '215px',
                position: 'absolute',
                transform: 'translate(-330px,146px)',
              }}
              src={`/img/${url}`}
              alt=""
            />
            <div
              className="bg-info"
              style={{ position: 'absolute', bottom: '0' }}
            >
              <h2>{name}</h2>
              <p>{desc}</p>
              <h4>{price}</h4>
            </div>
          </div>
          <div
            onClick={() => {
              this.props.handleGlassesItemClick(this.props.detail);
            }}
          ></div>
          <div className="col-5">
            <img
              style={{ width: '100%', position: 'relative' }}
              src="/img/glassesImage/model.jpg"
              alt=""
            />

            <img
              style={{
                width: '215px',
                position: 'absolute',
                transform: 'translate(-330px,146px)',
              }}
              src={`/img/${url}`}
              alt=""
            />
          </div>
        </div>
      </>
    );
  }
}
